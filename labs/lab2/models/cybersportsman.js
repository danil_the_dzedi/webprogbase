/**
 * @typedef Cybersportsman
 * @property {integer} ID
 * @property {string} fullname
 * @property {integer} age
 * @property {string} nickname
 * @property {string} discipline
 * @property {string} team
 * @property {string} lastJoinDate // date
 * @property {integer} prize
 */
class Cybersportsman
{
    constructor(ID, fullname, age, nickname, discipline, team, lastJoinDate, prize) // toadd MORE
    {
        this.ID = ID;
        this.fullname = fullname;
        this.age = age;
        this.nickname = nickname;
        this.discipline = discipline;
        this.team = team;
        this.lastJoinDate = lastJoinDate;
        this.prize = prize;
    }
}
module.exports = Cybersportsman;