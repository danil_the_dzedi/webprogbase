/**
 * @typedef User
 * @property {integer} ID
 * @property {string} login
 * @property {string} fullname
 * @property {string} role
 * @property {string} registeredAt // date
 * @property {string} avatarURL
 * @property {boolean} isEnabled
*/
class User
{
    constructor(ID, login, fullname, role, registeredAt, avatarURL, isEnabled)
    {
        this.ID = ID;
        this.login = login;
        this.fullname = fullname;
        this.role = role;
        this.registeredAt = registeredAt;
        this.avatarURL = avatarURL;
        this.isEnabled = isEnabled;
    }
}
module.exports = User;