const express = require('express');
const Router = express.Router();
const User = require('../models/user');
const userController = require('../controllers/userController');

/**
 * returns all users
 * @route GET /api/users
 * @param {integer} page.query
 * @param {integer} per_page.query
 * @group Users - user operations
 * @returns {Array.<User>} User - all users
 * @returns {Error} 400 - Bad request
 */
Router.get('', userController.getUsers);

/**
 * returns specified user by ID
 * @route GET /api/users/{id}
 * @param {integer} id.path.required - id of the User - eg: 1
 * @group Users - user operations
 * @returns {User.model} 200 - User found
 * @returns {Error} 404 - User not found
 */
Router.get('/:id', userController.getUserByID);

/**
 * creates new user
 * @route POST /api/users
 * @param {User.model} user.body.required - new User object
 * @group Users - user operations
 * @returns {User.model} 201 - User created
 */
Router.post('', userController.addUser);

/**
 * updates existing user
 * @route PUT /api/users
 * @param {User.model} user.body.required - changing User object
 * @group Users - user operations
 * @returns {User.model} 200 - User changed
 * @returns {Error} 404 - User not found
 */
Router.put('', userController.updateUser);

/**
 * deletes existing user
 * @route DELETE /api/users/{id}
 * @param {integer} id.path.required - id of the User - eg: 3
 * @group Users - user operations
 * @returns {User.model} 200 - User deleted
 * @returns {Error} 404 - User not found
 */
Router.delete('/:id', userController.deleteUser);

module.exports = Router;