const express = require('express');
const Router = express.Router();
const mediaController = require('../controllers/mediaController');
const multer = require('multer');
const upload = multer({ dest: './uploads' });

/**
 * returns existing image
 * @route GET /api/media/{id}
 * @param {integer} id.path.required - id of the Image - eg 2
 * @group Media - upload and get images
 * @returns {file} 200 - Image found
 * @returns {Error} 404 - Image not found
 */
Router.get('/:id', mediaController.getMediaByID);

/**
 * creates new image
 * @route POST /api/media
 * @group Media - upload and get images
 * @consumes multipart/form-data
 * @param {file} image.formData.required - uploading image
 * @returns {integer} 200 - Added image ID
 * @returns {Error} 400 - Bad request: wrong image format
 */
Router.post('', upload.single('image'), mediaController.addMedia);

module.exports = Router;