const express=require('express');
const userRouter = require('../routes/userRouter');
const cybersportsmanRouter = require('../routes/cybersportsmenRouter');
const mediaRouter = require('../routes/mediaRouter');
const Router = express.Router();
Router.use('/users', userRouter);
Router.use('/players', cybersportsmanRouter);
Router.use('/media', mediaRouter);
module.exports = Router;
