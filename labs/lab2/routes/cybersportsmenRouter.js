const express = require('express');
const Router = express.Router();
const Cybersportsman = require('../models/cybersportsman');
const cybersportsmanController = require('../controllers/cybersportsmanController');

/**
 * returns all players
 * @route GET /api/players
 * @param {integer} page.query
 * @param {integer} per_page.query
 * @group Players - cybersportsmen operations
 * @returns {Array.<Cybersportsman>} Player - all players
 * @returns {Error} 400 - Bad request
 */
Router.get('', cybersportsmanController.getPlayers);

/**
 * returns certain player by ID
 * @route GET /api/players/{id}
 * @param {integer} id.path.required - id of the player - eg 2
 * @group Players - cybersportsmen operations
 * @returns {Cybersportsman.model} 200 - Player found
 * @returns {Error} 404 - Player not found
 */
Router.get('/:id', cybersportsmanController.getPlayerByID)

/**
 * creates new player
 * @route POST /api/players
 * @param {Cybersportsman.model} player.body.required - new Player object
 * @group Players - cybersportsmen operations
 * @returns {Cybersportsman.model} 201 - Player created
 */
Router.post('', cybersportsmanController.addPlayer);

/**
 * updates existing player
 * @route PUT /api/players
 * @param {Cybersportsman.model} player.body.required - changing Player object
 * @group Players - cybersportsmen operations
 * @returns {Cybersportsman.model} 200 Player changed
 * @returns {Error} 404 - Player not found
 */
Router.put('', cybersportsmanController.updatePlayer);

/**
 * deletes existing player
 * @route DELETE /api/players/{id}
 * @param {integer} id.path.required - id of the player - eg 3
 * @group Players - cybersportsmen operations
 * @returns {Cybersportsman.model} 200 Player deleted
 * @returns {Error} 404 - Player not found
 */
Router.delete('/:id', cybersportsmanController.deletePlayer);

module.exports = Router;