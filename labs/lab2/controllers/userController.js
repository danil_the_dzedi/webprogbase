const User = require('../models/user');
const UserRepository = require('../repositories/userRepository');
const userRepo = new UserRepository('/home/danil/Desktop/Coronavirus/Course2/WebP/webprogbase/labs/lab2/data/users.json');
module.exports =
{
    getUsers(req, res)
    {
        const users = userRepo.getUsers();
        if (req.query.page && req.query.per_page)
        {
            if (req.query.page > Math.ceil(users.length / req.query.per_page) || req.query.page <= 0 || req.query.per_page <= 0)
            {
                res.status(400).send('Bad request: wrong parameters');
                res.end();
                return;
            }
            else
            {
                const skippedItemsCount = (req.query.page - 1) * req.query.per_page;
                let result = [];
                for (let i = skippedItemsCount; users[i] != null && result.length < req.query.per_page; i++)
                {
                    result.push(users[i]);
                }
                res.send(result);
                res.end();
                return;
            }
        }
        res.send(users);
        res.end();
    },
    getUserByID(req, res)
    {
        const User = userRepo.getUserByID(parseInt(req.params.id));
        if (User)
        {
            res.send(User);
        }
        else
        {
            res.status(404).send(`Not found user {ID:${req.params.id}}`);
        }
        res.end();
    },
    addUser(req, res)
    {
        const addedID = userRepo.addUser(req.body);
        res.send(userRepo.getUserByID(addedID));
        res.end();
    },
    updateUser(req, res)
    {
        if (userRepo.updateUser(req.body))
        {
            res.send(req.body);
        }
        else
        {
            res.status(404).send(`Not foudn user {ID:${req.body.ID}}`);
        }
        res.end();
    },
    deleteUser(req, res)
    {
        const user = userRepo.getUserByID(parseInt(req.params.id));
        if (user)
        {
            userRepo.deleteUser(user.ID);
            res.send(user);
        }
        else
        {
            res.status(404).send(`Not found user {ID:${req.params.id}}`);
        }
        res.end();
    }
}