const Cybersportsman = require('../models/cybersportsman');
const cybersportsmanRepository = require('../repositories/cybersportsmenRepository');
const playerRepo = new cybersportsmanRepository('/home/danil/Desktop/Coronavirus/Course2/WebP/webprogbase/labs/lab2/data/cybersportsmen.json');
const bodyParser = require('body-parser');
module.exports =
{
    getPlayers(req, res)
    {
        const players = playerRepo.getPlayers();
        if (req.query.page && req.query.per_page)
        {
            if (req.query.page > Math.ceil(players.length / req.query.per_page) || req.query.page <= 0 || req.query.per_page <= 0)
            {
                res.status(400).send('Bad request: wrong parameters');
                res.end();
                return;
            }
            else
            {
                const skippedItemsCount = (req.query.page - 1) * req.query.per_page;
                let result = [];
                for (let i = skippedItemsCount; players[i] != null && result.length < req.query.per_page; i++)
                {
                    result.push(players[i]);
                }
                res.send(result);
                res.end();
                return;
            }
        }
        res.send(players);
        res.end();
    },
    getPlayerByID(req, res)
    {
        const Player = playerRepo.getPlayerByID(parseInt(req.params.id));
        if (Player)
        {
            res.send(Player);
        }
        else
        {
            res.status(404).send(`Not found player {ID:${req.params.id}}`);
        }
        res.end();
    },
    addPlayer(req, res)
    {
        const addedID = playerRepo.addPlayer(req.body);
        res.status(201).send(playerRepo.getPlayerByID(addedID));
        res.end();
    },
    updatePlayer(req, res)
    {
        if (playerRepo.updatePlayer(req.body))
        {
            res.send(req.body);
        }
        else
        {
            res.status(404).send(`Not found player {ID:${req.body.ID}}`);
        }
        res.end();
    },
    deletePlayer(req, res)
    {
        const player = playerRepo.getPlayerByID(parseInt(req.params.id));
        if (player)
        {
            playerRepo.deletePlayer(player.ID);
            res.send(player);
        }
        else
        {
            res.status(404).send(`Not found player {ID:${req.params.id}}`);
        }
        res.end();
    }
}