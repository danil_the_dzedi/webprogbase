const MediaRepository = require('../repositories/mediaRepository');
const mediaDirectory = '/home/danil/Desktop/Coronavirus/Course2/WebP/webprogbase/labs/lab2/data/media';
const mediaRepo = new MediaRepository(mediaDirectory, mediaDirectory + '/id.json');

module.exports =
{
    getMediaByID(req, res)
    {
        const imagePath = mediaRepo.getMediaByID(parseInt(req.params.id));
        console.log(imagePath);
        if (imagePath)
        {
            res.sendFile(imagePath);
        }
        else
        {
            res.status(404).send(`Not found Media {ID:${req.params.id}}`);
        }
    },
    addMedia(req, res)
    {
        if (req.file.mimetype.split('/')[0] == 'image')
        {
            const resultingPath = mediaRepo.addMedia(req.file.path);
            const pathElements = resultingPath.split('/');
            res.send({ID: pathElements[pathElements.length - 1], path: resultingPath});
        }
        else
        {
            res.status(400).send('Bad request: wrong file foramt');
        }
        res.end();
    }
}