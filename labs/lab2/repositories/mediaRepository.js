const fs = require('fs');
const MediaStorage = require('../mediaStorage');
class MediaRepository
{
    constructor(mediaDirectory, idFile)
    {
        this.mediaStorage = new MediaStorage(mediaDirectory, idFile);
    }
    getMediaByID(ID)
    {
        const imagePath = this.mediaStorage.mediaDirPath + `/${ID}`;
        if (fs.existsSync(imagePath))
        {
            return imagePath;
        }
        return undefined;
    }
    addMedia(newFilePath)
    {
        const resultingPath = this.mediaStorage.mediaDirPath + `/${this.mediaStorage.getNextID()}`;
        fs.renameSync(newFilePath, resultingPath);
        this.mediaStorage.incrementNextID();
        return resultingPath;
    }
}
module.exports = MediaRepository;