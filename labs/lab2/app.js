const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

 
const apiRouter = require('./routes/api');
app.use('/api', apiRouter);
 
const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);
 
const options = {
    swaggerDefinition: {
        info: {
            description: 'TODO: Change this description',
            title: 'TODO: Change this title',
            version: '1.0.0',
        },
        host: 'localhost:3000',
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes//*.js', './models//*.js'],
};
expressSwagger(options);
 
app.listen(3000, function() {console.log('Server is running')});