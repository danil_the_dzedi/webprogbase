const fs = require('fs');
class MediaStorage
{
    constructor(mediaDirectory, idFile)
    {
        this.mediaDirPath = mediaDirectory;
        this.idFilePath = idFile;
    }
    getNextID()
    {
        const idObject = JSON.parse(fs.readFileSync(this.idFilePath));
        return idObject.nextID;
    }
    incrementNextID()
    {
        let nextID = this.getNextID();
        nextID++;
        fs.writeFileSync(this.idFilePath, JSON.stringify({nextID: nextID}, null, 4));
    }
}
module.exports = MediaStorage;