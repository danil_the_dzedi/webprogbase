const readlineSync = require("readline-sync");
const UserRepository = require("./repositories/userRepository");
const CybersportsmanRepository = require("./repositories/cybersportsmenRepository");
const User = require("./models/user");
const Cybersportsman = require("./models/cybersportsman");

const userRepo = new UserRepository("./data/users.json");
const playerRepo = new CybersportsmanRepository("./data/cybersportsmen.json");
while (true) // TODO: exit command + clear command
{
    const input = readlineSync.question("> Input command:\n> ").trim();
    const parts = input.split(" ").filter(e => e.length > 0);
    const [command, model, ...options] = parts; // TODO: replace 'options' on something else
    if (command === "get")
    {
        if (model === "user" && options.length === 1)
        {
            if (!isNaN(options[0]) && options[0] - Math.trunc(options[0]) === 0 && options[0] >= 1)
            {
                const user = userRepo.getUserByID(parseInt(options[0]));
                if (!user)
                {
                    console.log(`User with id '${options[0]}' was not found`);
                    continue;
                }
                showFormattedUser(user);
            }
            else
            {
                console.log(`Invalid '${command + " " + model}' command: ID is a positive integer number`);
            }
        }
        else if (model === "users" && options.length === 0)
        {
            showFormattedUsers(userRepo.getUsers());
        }
        else if (model === "player" && options.length === 1)
        {
            if (!isNaN(options[0]) && options[0] - Math.trunc(options[0]) === 0 && options[0] >= 1)
            {
                const player = playerRepo.getPlayerByID(parseInt(options[0]));
                if (!player)
                {
                    console.log(`Player with id '${options[0]}' was not found`);
                    continue;
                }
                showFormattedPlayer(player);
            }
            else
            {
                console.log(`Invalid '${command + " " + model}' command: ID is a positive integer number`);
            }
        }
        else if (model === "players" && options.length === 0)
        {
            showFormattedPlayers(playerRepo.getPlayers());
        }
        else
        {
            console.log(`Invalid '${command}' command: wrong options number (1:model,2:ID expected) / wrong model, choose [users / user <id> / players / player <id>]`);
        }   
    }
    else if (command === "create")
    {
        if (options.length === 0)
        {
            if (model === "player")
            {
                const inputtedPlayer = takePlayerInfo();
                const confirmed = takeCorrectBolean("> Are you sure you want to add this player? [y/n]\n");
                if (confirmed)
                {
                    playerRepo.addPlayer(inputtedPlayer);
                    console.log(`New player has been created`);
                }
            }
            else if (model === "user")
            {
                const inputtedUser = takeUserInfo();
                const confirmed = takeCorrectBolean("> Are you sure you want to add this user? [y/n]\n");
                if (confirmed)
                {
                    userRepo.addUser(inputtedUser);
                    console.log(`New user has been created`);
                }
            }
            else
            {
                console.log(`Invalid '${command}' command: wrong model, choose [user / player]`);
            }
        }
        else
        {
            console.log(`Invalid '${command}' command: wrong options number (0 expected)`);
        }
    }
    else if (command === "delete")
    {
        if (options.length === 1)
        {
            if (!isNaN(options[0]) && options[0] - Math.trunc(options[0]) === 0 && options[0] >= 1)
            {
                if (model === "player")
                {
                    const confirmed = takeCorrectBolean("> Are you sure you want to remove this player? [y/n]\n");
                    if (confirmed)
                    {
                        if (playerRepo.deletePlayer(parseInt(options[0])))
                        {
                            console.log(`Player with ID = ${options[0]} has been removed`);
                        }
                        else
                        {
                            console.log(`Player with ID = ${options[0]} wasn't found`);
                        }
                    }
                }
                else if (model === "user")
                {
                    const confirmed = takeCorrectBolean("> Are you sure you want to remove this user? [y/n]\n");
                    if (confirmed)
                    {
                        if (userRepo.deleteUser(parseInt(options[0])))
                        {
                            console.log(`User with ID = ${options[0]} has been removed`);
                        }
                        else
                        {
                            console.log(`User with ID = ${options[0]} wasn't found`);
                        }
                    }
                }
                else
                {
                    console.log(`Invalid '${command}' command: wrong model, choose [player <id> / user <id>]`);
                }
            }
            else
            {
                console.log(`Invalid '${command + " " + model}' command: ID is a positive integer number`);
            }
        }
        else
        {
            console.log(`Invalid '${command}' command: wrong arguments number (1:model,2:ID expected)`);
        }
    }
    else if (command === "update")
    {
        if (options.length === 1)
        {
            if (!isNaN(options[0]) && options[0] - Math.trunc(options[0]) === 0 && options[0] >= 1)
            {
                if (model === "player")
                {
                    const player = playerRepo.getPlayerByID(parseInt(options[0]));
                    if (!player)
                    {
                        console.log(`Player with id '${options[0]}' was not found`);
                        continue;
                    }
                    showFormattedPlayer(player);
                    const update = takePlayerInfo();
                    update.ID = player.ID;
                    const confirmed = takeCorrectBolean("Are you sure you want to save changes to this player? [y/n]\n");
                    if (confirmed)
                    {
                        playerRepo.updatePlayer(update);
                        console.log(`Changes to player with ID = ${options[0]} have been saved`);
                    }
                    else
                    {
                        console.log(`Changes to player with ID = ${options[0]} have been cancelled`);
                    }
                }
                else if (model === "user")
                {
                    const user = userRepo.getUserByID(parseInt(options[0]));
                    if (!user)
                    {
                        console.log(`User with id '${options[0]}' was not found`);
                        continue;
                    }
                    showFormattedUser(user);
                    const update = takeUserInfo();
                    update.ID = user.ID;
                    const confirmed = takeCorrectBolean("Are you sure you want to save changes to this user? [y/n]\n");
                    if (confirmed)
                    {
                        userRepo.updateUser(update);
                        console.log(`Changes to user with ID = ${options[0]} have been saved`);
                    }
                    else
                    {
                        console.log(`Changes to user with ID = ${options[0]} have been cancelled`);
                    }
                }
                else
                {
                    console.log(`Invalid '${command}' command: wrong model, choose [user / player]`);
                }
            }
            else
            {
                console.log(`Invalid '${command + " " + model}' command: ID is a positive integer number`);
            }
        }
        else
        {
            console.log(`Invalid '${command}' command: wrong options number (1:model,2:ID expected)`);
        }
    }
    else
    {
        console.log(`Invalid command: no such command '${command}' found`);
    }
}
function showFormattedUser(user)
{
    console.log(`---< Login: ${user.login} >---`);
    console.log(`---< Fullname: ${user.fullname} >---`);
    console.log(`---< Registration: ${user.registeredAt} >---`);
    const admin = user.role ? "yes" : "no";
    console.log(`---< Admin: ${admin} >---`);
    console.log(`---< Avatar: ${user.avatarURL} >---`);
    console.log(`---< Banned: ${user.isEnabled} >---`);
}
function showFormattedUsers(users)
{
    console.log("(1) - Login   (2) - Fullname   (3) - Registration date");
    for (const [index, value] of users.entries())
    {
        console.log(`${index + 1}. ${value.login}   ${value.fullname}   ${value.registeredAt}`);
    }
}
function showFormattedPlayer(player)
{
    console.log(`---< Fullname: ${player.fullname} >---`);
    console.log(`---< Age: ${player.age} >---`);
    console.log(`---< Nickname: ${player.nickname} >---`);
    console.log(`---< Dsicipline: ${player.discipline} >---`);
    console.log(`---< Team: ${player.team} >---`);
    console.log(`---< Joined: ${player.lastJoinDate} >---`);
    console.log(`---< Prize: $${player.prize} >---`);
}
function showFormattedPlayers(players)
{
    console.log("(1) - [Team]   (2) - Nickname   (3) - (Fullname)");
    for (const [index, value] of players.entries())
    {
        console.log(`${index + 1}. [${value.team}] ${value.nickname} (${value.fullname})`);
    }
}
function takePlayerInfo() // TODO: modify using readlineSync options;
{
    console.log("> Enter player data");
    const addedPlayer = new Cybersportsman();
    addedPlayer.fullname = readlineSync.question("Fullname: ");
    addedPlayer.age = takeCorrectInteger("Age: ", 16, 60);
    addedPlayer.nickname = readlineSync.question("Nickname: ");
    addedPlayer.discipline = readlineSync.question("Discipline: ");
    addedPlayer.team = readlineSync.question("Team: ");
    addedPlayer.lastJoinDate = takeCorrectISODate("Last join date:\n", addedPlayer.age - 16);
    addedPlayer.prize = takeCorrectInteger("Prize: ", 0);
    return addedPlayer;
}
function takeUserInfo()
{
    console.log("> Enter user data");
    const addedUser = new User();
    addedUser.login = readlineSync.question("Login: ");
    addedUser.fullname = readlineSync.question("Fullname: ");
    addedUser.role = takeCorrectBolean("Admin?: ");
    addedUser.registeredAt = takeCorrectISODate("Created:\n");
    addedUser.avatarURL = "https://steamuserimages-a.akamaihd.net/ugc/885384897182110030/F095539864AC9E94AE5236E04C8CA7C2725BCEFF/";
    addedUser.isEnabled = true;
    return addedUser;
}
function takeCorrectInteger(msg, start = Number.MIN_SAFE_INTEGER, limit = Number.MAX_SAFE_INTEGER)
{
    while (true)
    {
        const result = readlineSync.questionInt(msg);
        if (result >= start && result <= limit)
        {
            return result;
        }
        console.log(`Value should satisfy (${start} <= value <= ${limit})`);
    }
}
function takeCorrectISODate(msg, ageFactor = (new Date().getFullYear() - 1970))
{
    console.log(msg);
    const joinDate = new Date();
    const currentYear = joinDate.getFullYear();
    joinDate.setFullYear(takeCorrectInteger("(Year): ", currentYear - ageFactor, currentYear));
    joinDate.setMonth(takeCorrectInteger("(Month): ", 1, 12) - 1);
    const month = joinDate.getMonth(); // 0,2,4,6,7,9,11 = 31, 1 = 28, 3,5,8,10
    if ((month % 2 === 0 && month < 8 && month !== 2) || (month % 2 !== 0 && month > 6)) // 31 
    {
        joinDate.setDate(takeCorrectInteger("(Day): ", 1, 31));
    }
    else if (month === 1) // 28-29
    {
        const installedYear = joinDate.getFullYear();
        if (installedYear % 4 !== 0 || (installedYear % 100 === 0 && installedYear % 400 !== 0)) // default
        {
            joinDate.setDate(takeCorrectInteger("(Day): ", 1, 28));
        }
        else // special
        {
            joinDate.setDate(takeCorrectInteger("(Day): ", 1, 29));
        }
    }
    else // 30
    {
        joinDate.setDate(takeCorrectInteger("(Day): ", 1, 30));
    }
    return joinDate.toISOString();
}
function takeCorrectBolean(msg)
{
    while (true)
    {
        const input = readlineSync.question(msg).toLowerCase();
        if (input === "yes" || input === "y" || input === "+" || input === "true" || input === "1")
        {
            return true;
        }
        else if (input === "no" || input === "n" || input === "-" || input === "false" || input === "0")
        {
            return false;
        }
        console.log("Choose [y/n] or [yes/no] or [+/-] or [1/0] or [true/false]");
    }
}