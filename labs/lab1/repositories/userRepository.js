const User = require("./../models/user");
const JsonStorage = require("./../jsonStorage");
class UserRepository 
{
    constructor(filePath) 
    {
        this.storage = new JsonStorage(filePath);
    }
    getUsers() 
    { 
        return this.storage.readObject().items;
    }
    getUserByID(ID) 
    {
        const items = this.storage.readObject().items;
        for (const item of items) 
        {
            if (item.ID === ID) 
            {
                return new User(item.ID, item.login, item.fullname, item.role, item.registeredAt, item.avatarURL, item.isEnabled);
            }
        }
        return null;
    }
    addUser(addingUser)
    {
        const users = this.getUsers();
        addingUser.ID = this.storage.nextID;
        this.storage.incrementNextID();
        users.push(addingUser);
        this.storage.writeObject(users);
    }
    updateUser(updatingUser)
    {
        if (this.getUserByID(updatingUser.ID))
        {
            const users = this.getUsers();
            const index = users.findIndex(e => e.ID === updatingUser.ID);
            users[index] = updatingUser;
            this.storage.writeObject(users);
            return true;
        }
        return false;
    }
    deleteUser(ID)
    {
        if (this.getUserByID(ID))
        {
            const users = this.getUsers();
            const index = users.findIndex(e => e.ID === ID);
            users.splice(index, 1);
            this.storage.writeObject(users);
            return true;
        }
        return false;
    }
};
module.exports = UserRepository;