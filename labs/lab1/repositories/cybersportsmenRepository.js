const Cybersportsman = require("./../models/cybersportsman");
const JsonStorage = require("./../jsonStorage");
class CybersportsmanRepository
{
    constructor(filePath) 
    {
        this.storage = new JsonStorage(filePath);
    }
    getPlayers() 
    { 
        return this.storage.readObject().items;
    }
    getPlayerByID(ID) 
    {
        const items = this.storage.readObject().items;
        for (const item of items) 
        {
            if (item.ID === ID) 
            {
                return new Cybersportsman(item.ID, item.fullname, item.age, item.nickname, item.discipline, item.team, item.lastJoinDate, item.prize);
            }
        }
        return null;
    }
    addPlayer(addingPlayer)
    {
        const players = this.getPlayers();
        addingPlayer.ID = this.storage.nextID;
        this.storage.incrementNextID();
        players.push(addingPlayer);
        this.storage.writeObject(players);
    }
    updatePlayer(updatingPlayer)
    {
        if (this.getPlayerByID(updatingPlayer.ID))
        {
            const players = this.getPlayers();
            const index = players.findIndex(e => e.ID === updatingPlayer.ID);
            players[index] = updatingPlayer;
            this.storage.writeObject(players);
            return true;
        }
        return false;
    }
    deletePlayer(ID)
    {
        if (this.getPlayerByID(ID))
        {
            const players = this.getPlayers();
            const index = players.findIndex(e => e.ID === ID);
            players.splice(index, 1);
            this.storage.writeObject(players);
            return true;
        }
        return false;
    }
};
module.exports = CybersportsmanRepository;