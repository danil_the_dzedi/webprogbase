const fs = require("fs");
class JsonStorage 
{
    constructor(filePath) 
    {
        this.filePath = filePath;
    }

    get nextID() 
    {
        return this.jsonObject.nextID;
    }

    incrementNextID() 
    {
        this.jsonObject.nextID++;
    }

    readObject() 
    {
        const jsonText = fs.readFileSync(this.filePath);
        this.jsonObject = JSON.parse(jsonText.toString());
        return this.jsonObject;
    }

    writeObject(items)
    {
        this.jsonObject.items = items;
        const jsonText = JSON.stringify(this.jsonObject, null, 4);
        fs.writeFileSync(this.filePath, jsonText);
    }
};
module.exports = JsonStorage;
